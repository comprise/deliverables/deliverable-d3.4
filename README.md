# Speech-to-text Software Libraries

Final library is divided into two parts for the speech-to-text (STT) task; namely, [acoustic model (AM) personalization](https://gitlab.inria.fr/comprise/deliverables/deliverable-d3.4/-/tree/master/AM_personalization) and [language model (LM) adaptation](https://gitlab.inria.fr/comprise/deliverables/deliverable-d3.4/-/tree/master/LM_adaptation).

## Acoustic Model Personalization

AM personalization software is mainly relevant to the [previous library](https://gitlab.inria.fr/comprise/deliverables/deliverable_d32) except for the extraction of accent embeddings. In this version, the proposed embeddings are obtained from all previous samples of a particular user. This version also provides a more refined code structure.

## Language Model Adaptation

LM adaptation software defines comprehensive guidelines for the final version of the STT personalization in COMPRISE. It contains personalization strategies for LM adaptation and major components are designed to match built-in Kaldi tools and binaries specifically. This also provides benefits in terms of complexity for a better deployment to personal devices.
