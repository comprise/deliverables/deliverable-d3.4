# KALDI Version Hash:
# git log -1 --format="%h"
# Date: Tue Sep 8, 2020
# 2b627b3578

export KALDI_ROOT="/home/mturan/misc/kaldi-master"
export KALDI_LM_ROOT="$KALDI_ROOT/tools/kaldi_lm"
export WD=`readlink -f .`
export PATH=$WD:$WD/utils:$KALDI_ROOT/src/bin:$KALDI_LM_ROOT/:$PATH
export LD_LIBRARY_PATH=$KALDI_ROOT/openfst/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}

export LC_ALL=C

export SRILM=$KALDI_ROOT/tools/srilm
export PATH=${PATH}:${SRILM}/bin:${SRILM}/bin/i686-m64
export SEQUITUR="$KALDI_ROOT/tools/sequitur-g2p"
export PATH="$PATH:${SEQUITUR}/bin"
export PYTHONPATH="${PYTHONPATH:-}:$SEQUITUR/./lib/python2.7/site-packages"

export IRSTLM="$KALDI_ROOT/tools/irstlm"
export PATH=${IRSTLM}/bin:${PATH}

. $KALDI_ROOT/tools/config/common_path.sh
