#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

set -euo pipefail

wdir=data/ami_ner
. ./utils/parse_options.sh

mkdir -p $wdir

amiurl=http://groups.inf.ed.ac.uk/ami/AMICorpusAnnotations
version=ami_public_manual_1.6.2
annot=$wdir/$version

[ ! -f $annot.zip ] && wget -nv -O $annot.zip $amiurl/$version.zip &> /dev/null

if [ ! -d $wdir/nxt ]; then
    echo "--> $0: downloading NXT annotation tool.."
    wget -q -O $wdir/nxt.zip \
        http://sourceforge.net/projects/nite/files/nite/nxt_1.4.4/nxt_1.4.4.zip
    [ ! -s $wdir/nxt.zip ] && echo "download failed! ($wdir/nxt.zip)" && exit 1
    unzip -d $wdir/nxt $wdir/nxt.zip &> /dev/null
fi

if [ ! -d $wdir/annotations ]; then
    echo "--> $0: extracting AMI transcriptions.."
    mkdir -p $wdir/annotations
    unzip -o -d $wdir/annotations $annot.zip &> /dev/null
fi

nxtlib=$wdir/nxt/lib
export CLASSPATH=".:$nxtlib:$nxtlib/nxt.jar:$nxtlib/jdom.jar:$nxtlib/xalan.jar:$nxtlib/xercesImpl.jar:$nxtlib/xml-apis.jar"

mkdir -p $wdir/query
meta="$wdir/annotations/AMI-metadata.xml"
java CountQueryResults -corpus $meta -q '($n named-entity):' \
    > $wdir/query/count_ner.log

split_list=$(grep -f <(awk '{print $1}' $wdir/query/count_ner.log) list/* | \
    sed 's/.orig//g' | cut -f2 -d'_')

paste <(echo "$split_list" | sort -k2 -t':' | cut -f1 -d':') \
    <(sort -k1 $wdir/query/count_ner.log) | sort -nr -k 3 | column -t \
    > $wdir/query/ner_stats.log

java FunctionQuery -corpus $wdir/annotations/AMI-metadata.xml -q '($n named-entity)' \
    -atts obs who '$n' '@extract(($w ne-type):$n>$w,name,0,0)' \
    > $wdir/query/ner.log

echo "--> $0: $wdir folder is successfully created"
exit 0
