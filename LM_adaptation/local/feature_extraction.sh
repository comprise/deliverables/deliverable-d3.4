#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

. ./cmd.sh
. ./path.sh

set -euo pipefail

# you can increase "nj" based on your CPU usage
nj=4

echo "==> running $0 $@.."

echo "--> $0: start 13-dim mfcc extraction"
for dset in train dev test; do
    utils/data/modify_speaker_info.sh --seconds-per-spk-max "120" \
        data/$dset data/split/$dset || exit 1
    steps/make_mfcc.sh --nj "$nj" --cmd "$train_cmd" data/split/$dset || exit 1
    steps/compute_cmvn_stats.sh data/split/$dset || exit 1
    utils/fix_data_dir.sh data/split/$dset || exit 1
done

echo "--> $0: start 40-dim mfcc extraction"
if [ -f data/split/train_sp_hires/feats.scp ]; then
    echo "--> $0: data/split/train_sp_hires/feats.scp exists" && exit 1
fi
echo "--> $0: prepare directory for high-resolution speed-perturbed data"
utils/data/perturb_data_dir_speed_3way.sh data/split/train data/split/train_sp

echo "--> $0: prepare data structure for the high-resolution MFCC features"
for datadir in train_sp test dev; do
    utils/copy_data_dir.sh data/split/$datadir data/split/${datadir}_hires
done

echo "--> $0: do volume-perturbation on training data before extraction"
utils/data/perturb_data_dir_volume.sh data/split/train_sp_hires || exit 1

echo "--> $0: extract high-resolution MFCC features"
for datadir in train_sp test dev; do
    steps/make_mfcc.sh --nj "$nj" --mfcc-config conf/mfcc_hires.conf \
        --cmd "$train_cmd" data/split/${datadir}_hires || exit 1
    steps/compute_cmvn_stats.sh data/split/${datadir}_hires || exit 1
    utils/fix_data_dir.sh data/split/${datadir}_hires || exit 1
done

echo "--> $0: extract low-res. speed-perturbed data for future alignments"
steps/make_mfcc.sh --nj "$nj" --cmd "$train_cmd" data/split/train_sp || exit 1
steps/compute_cmvn_stats.sh data/split/train_sp || exit 1
utils/fix_data_dir.sh data/split/train_sp || exit 1

echo "--> $0: start i-vector extraction.."
echo "--> $0: computing a subset of data to train the diagonal UBM"
mkdir -p data/ivector/diag_ubm
temp_data_root=data/ivector/diag_ubm

# train a diagonal UBM using a subset of about a quarter of the data
num_utts_total=$(wc -l data/split/train_sp_hires/utt2spk | cut -f1 -d' ')
num_utts=$[$num_utts_total/4]
utils/data/subset_data_dir.sh data/split/train_sp_hires $num_utts \
    ${temp_data_root}/train_sp_hires_subset || exit 1

echo "--> $0: computing a PCA transform from the hires data"
steps/online/nnet2/get_pca_transform.sh --cmd "$train_cmd" \
    --splice-opts "--left-context=3 --right-context=3" \
    --max-utts 10000 --subsample 2 \
    ${temp_data_root}/train_sp_hires_subset \
    data/ivector/pca_transform || exit 1

echo "--> $0: training the diagonal UBM"
# use 512 gaussians in the UBM
steps/online/nnet2/train_diag_ubm.sh --cmd "$train_cmd" --nj $nj \
    --num-frames 700000 --num-threads "32" \
    ${temp_data_root}/train_sp_hires_subset 512 \
    data/ivector/pca_transform \
    data/ivector/diag_ubm || exit 1

echo "--> $0: training the i-vector extractor"
steps/online/nnet2/train_ivector_extractor.sh --cmd "$train_cmd" --nj "$nj" \
    data/split/train_sp_hires data/ivector/diag_ubm \
    data/ivector/extractor || exit 1

echo "--> $0: start i-vector extraction for train_sp, test and dev splits"
temp_data_root=data/ivector/train_sp_hires
utils/data/modify_speaker_info.sh --utts-per-spk-max 2 \
    data/split/train_sp_hires ${temp_data_root}/train_sp_hires_max2

steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj "$nj" \
    --ivector-period "25" ${temp_data_root}/train_sp_hires_max2 \
    data/ivector/extractor \
    data/ivector/train_sp_hires || exit 1

steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj "$nj" \
    --ivector-period "$ivec_period" data/split/test_hires \
    data/ivector/extractor \
    data/ivector/test_hires || exit 1

steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj "$nj" \
    --ivector-period "$ivec_period" data/split/dev_hires \
    data/ivector/extractor \
    data/ivector/dev_hires || exit 1

echo -e "\n==> $0: feature extraction is finished successfully"
exit 0
