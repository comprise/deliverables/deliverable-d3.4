#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

set -euo pipefail

# -------------------- START CONFIGURATION -------------------- #
nj=number-of-jobs
input_lang=data/lang/folder
data_folder=your/data/path
tree_dir=tree_sp/folder/from/chain/training
chain_lang=this/lang/folder/is/created/by/this/script
# --------------------- END CONFIGURATION --------------------- #

. ./utils/parse_options.sh
. ./path.sh

echo "--> $0 $@"

cmd=run.pl
chunk_left_context=0
chunk_right_context=0
chunk_width=140,100,160
frames_per_chunk=$(echo $chunk_width | cut -d, -f1)

mkdir -p $chain_lang
cp -r $input_lang/* $chain_lang
silphones=$(cat $chain_lang/phones/silence.csl) || exit 1
nonsilphones=$(cat $chain_lang/phones/nonsilence.csl) || exit 1

steps/nnet3/chain/gen_topo.py ${nonsilphones} ${silphones} > $chain_lang/topo

echo "--> $0: creating a decoding graph under $chain_lang/graph_tg folder.."
utils/mkgraph.sh --self-loop-scale 1.0 \
        $chain_lang $tree_dir $chain_lang/graph_tg || exit 1

echo "--> $0: start decoding for 'test' and 'dev' using $chain_lang/graph_tg.."
mkdir -p exp/${chain_lang#*_}
ln -s ${tree_dir}/../tdnn_sp/{final*,cmvn*,frame*,phones*} exp/${chain_lang#*_}
rm exp/${chain_lang#*_}/.error 2>/dev/null || true
for data in test dev; do
    (
        ivectors=$data_folder/ivector/${data}_hires
        steps/nnet3/decode.sh \
            --acwt 1.0 --post-decode-acwt 10.0 \
            --extra-left-context "$chunk_left_context" \
            --extra-right-context "$chunk_right_context" \
            --extra-left-context-initial 0 \
            --extra-right-context-final 0 \
            --frames-per-chunk "$frames_per_chunk" \
            --nj "$nj" --cmd "$cmd" --num-threads 4 \
            --online-ivector-dir "$ivectors" \
            $chain_lang/graph_tg \
            ${data_folder}/split/${data}_hires \
            exp/${chain_lang#*_}/decode_${data} || exit 1
    ) || touch exp/${chain_lang#*_}/.error &
done
wait
[[ -f exp/${chain_lang#*_}/.error ]] && \
    echo -e "\n==> $0: there was a problem in ${chain_lang#*_} decoding !" && exit 1

echo "--> $0: decoding under exp/${chain_lang#*_} folder is finished"
exit 0
