#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

. ./cmd.sh
. ./path.sh

set -euo pipefail

echo "==> running $0 $@.."

# required files for data/local/lang and data/lang:
# "extra_questions,nonsilence_phones,optional_silence,silence_phones,lexicon"
dir=data/local/dict
mkdir -p $dir

echo "Getting CMU dictionary"
svn co https://svn.code.sf.net/p/cmusphinx/code/trunk/cmudict $dir/cmudict

# silence phones, one per line
for w in sil laughter noise oov; do echo $w; done > $dir/silence_phones.txt
echo sil > $dir/optional_silence.txt

# prepare nonsilence_phones
cat $dir/cmudict/cmudict.0.7a.symbols | sed s/[0-9]//g | \
    perl -ane 's:\r::; print;' | sort | uniq > $dir/nonsilence_phones.txt

# an extra question will be added by including the silence phones in one class
cat $dir/silence_phones.txt| awk '{printf("%s ", $1);} END{printf "\n";}' > \
    $dir/extra_questions.txt || exit 1
grep -v ';;;' $dir/cmudict/cmudict.0.7a | \
    perl -ane 'if(!m:^;;;:){ s:(\S+)\(\d+\) :$1 :; s:  : :; print; }' | \
    perl -ane '@A = split(" ", $_); for ($n = 1; $n<@A;$n++) { $A[$n] =~ s/[0-9]//g; } print join(" ", @A) . "\n";' | \
    sort | uniq > $dir/lexicon1_raw_nosil.txt || exit 1

# limit the vocabulary to the predefined 50k words
wget -nv -O $dir/wordlist.50k.gz \
    http://www.openslr.org/resources/9/wordlist.50k.gz
gunzip -c $dir/wordlist.50k.gz > $dir/wordlist.50k
join $dir/lexicon1_raw_nosil.txt $dir/wordlist.50k \
    > $dir/lexicon1_raw_nosil_50k.txt

# add prons for laughter, noise, OOV
for w in `grep -v sil $dir/silence_phones.txt`; do
    echo "[$w] $w"
done | cat - $dir/lexicon1_raw_nosil_50k.txt > $dir/lexicon2_raw_50k.txt

# add some specific words, those are only with 100 missing occurences or more
(
    echo "MM M"; \
    echo "HMM HH M"; \
    echo "MM-HMM M HH M"; \
    echo "COLOUR  K AH L ER"; \
    echo "COLOURS  K AH L ER Z"; \
    echo "REMOTES  R IH M OW T Z"; \
    echo "FAVOURITE F EY V ER IH T"; \
    echo "<unk> oov"
) | cat - $dir/lexicon2_raw_50k.txt | sort -u > $dir/lexicon3_extra_50k.txt

cp $dir/lexicon3_extra_50k.txt $dir/lexicon.txt
rm $dir/lexiconp.txt 2>/dev/null || true

[ ! -f $dir/lexicon.txt ] && exit 1

get_oov_list () {
    data=$1
    cat data/ami/$data/text | awk '{for (n=2;n<=NF;n++){ count[$n]++; } } END { for(n in count) { print count[n], n; }}' | sort -nr \
        > $dir/${data}_counts

    awk '{print $1}' $dir/lexicon.txt | \
        perl -e '($word_counts)=@ARGV;
        open(W, "<$word_counts")||die "opening word-counts $word_counts";
        while(<STDIN>) { chop; $seen{$_}=1; }
        while(<W>) {
            ($c,$w) = split;
            if (!defined $seen{$w}) { print; }
        }' $dir/${data}_counts > $dir/${data}_oov
}

echo "--> $0: updating lexicon using 'github.com/Kyubyong/g2p' package.."
for f in train test dev; do
    get_oov_list $f
    awk '{print $2}' $dir/${f}_oov | python3 local/g2p_oov.py \
        > $dir/lexicon_${f}.txt
done

mv $dir/lexicon.txt $dir/lexicon_ami.txt
cat $dir/lexicon* | sort -u > $dir/lexicon_final.txt
paste <(awk '{print $1}' $dir/lexicon_final.txt) \
    <(cut -f2- -d' ' $dir/lexicon_final.txt | sed ''s:\'::g'') | \
    column -t | tr -s " " > $dir/lexicon.txt

rm -v data/local/dict/{*_counts,*_oov,lexicon_*.txt}
utils/validate_dict_dir.pl $dir

echo "--> $0: preparing 'data/lang' folder.."
utils/prepare_lang.sh $dir "<unk>" data/local/lang data/lang

echo -e "\n==> $0: lexicon and language creation is done successfully"
exit 0
