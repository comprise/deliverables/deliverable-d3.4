#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

. ./cmd.sh
. ./path.sh

set -euo pipefail

echo "==> $0: start kaldi-style folder processing for ami data.."
local/organize_ami_data.sh || exit 1

echo -e "\n==> $0: prepare lexicon and language related folders.."
local/make_dict.sh || exit 1

echo -e "==> $0: download AMI entity class tags using NXT annotation tool..\n"
local/extract_ami_ner.sh || exit 1

echo -e "\n==> $0: prepare data resource folder for NER classes..\n"
local/generate_resources.sh || exit 1

echo -e "\n==> $0: replacing NER words with their corresponding tags..\n"
cut -d ' ' -f 2- data/ami/train/text | replace-words-with-classes normalize=0 \
    outfile=data/wclass/ner_count.classes classes=data/wclass/ner.classes | \
    gzip -c > data/wclass/train.gz

echo -e "\n==> $0: randomly replace class labels with alternative replicates.."
# data/wclass/wclass_list.txt :
# C=ORG   0.0
# C=PER   0.0
# C=LOC   0.0
# C=MISC  0.0
out_fold=data/lm
mkdir -p $out_fold
gunzip -c data/wclass/train.gz | \
    python3 local/replace_entity.py data/wclass/wclass_list.txt \
        data/wclass/ner_count.classes | gzip -c > $out_fold/train.gz

echo e "\n==> $0: data preparation is successfully finished!"
exit 0
