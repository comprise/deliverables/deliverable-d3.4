#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

set -euo pipefail
. ./path.sh

# you can increase "nj" based on your CPU usage
nj=4

stage=0
cmd=run.pl
data_fold=data

# source gmm-dir for alignments
gmm=tri3
gmm_dir=exp/${gmm}
ali_dir=exp/${gmm}_align_train_sp

# ivector and tdnn related parameters
srand=123
num_epochs=15
ivec_period=25
remove_egs=true
train_stage=-10
num_threads_ubm=32
xent_regularize=0.1
chunk_left_context=0
chunk_right_context=0
chunk_width=140,100,160
dropout_schedule='0,0@0.20,0.5@0.50,0'
num_gpu=$(nvidia-smi --query-gpu=name --format=csv,noheader | wc -l)

# related folders for tdnn training
dir=exp/tdnn_sp
tree_dir=exp/tree_sp
train_data_dir=${data_fold}/split/train_sp_hires
lat_dir=exp/${gmm}_train_sp_lattices
lang=exp/lang_chain
lores_train_data_dir=${data_fold}/split/train_sp
train_ivector_dir=$data_fold/ivector/train_sp_hires

echo "--> $0: aligning with speed perturbed train data.."
steps/align_fmllr.sh --nj "$nj" --cmd "$cmd" \
    $data_fold/split/train_sp $data_fold/lang $gmm_dir $ali_dir

for f in $train_data_dir/feats.scp $train_ivector_dir/ivector_online.scp \
    $lores_train_data_dir/feats.scp $gmm_dir/final.mdl $ali_dir/ali.1.gz \
    $gmm_dir/final.mdl; do
    [[ ! -f $f ]] && echo "$0: expected file $f to exist" && exit 1
done

if [ $stage -le 0 ]; then
    # Create a version of the 'lang' that has one state per phone in the topo
    # file. [note: it really has two states, the first one is only repeated
    # once, the second one has zero or more repeats]
    mkdir -p $lang
    cp -r $data_fold/lang/* $lang
    silphones=$(cat $lang/phones/silence.csl) || exit 1
    nonsilphones=$(cat $lang/phones/nonsilence.csl) || exit 1
    steps/nnet3/chain/gen_topo.py $nonsilphones $silphones > $lang/topo
fi

if [ $stage -le 1 ]; then
    echo -e "\n--> generating lattices (lat.*.gz) with alignments.."
    # Note: use the same num-jobs as the alignments
    steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" \
        ${lores_train_data_dir} $lang $gmm_dir $lat_dir
    rm $lat_dir/fsts.*.gz
fi

if [ $stage -le 2 ]; then
    # Build a tree using our new topology. We have alignments for the
    # speed-perturbed data (local/ivector_extraction.sh made them)
    # The num-leaves is always somewhat less than the num-leaves from the GMM.
    if [ -f $tree_dir/final.mdl ]; then
        echo -e "\n--> $0: $tree_dir/final.mdl exists, refusing to overwrite !"
        exit 1
    fi

    steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
        --context-opts "--context-width=2 --central-position=1" --cmd "$cmd" \
        3500 ${lores_train_data_dir} $lang $ali_dir $tree_dir
fi

if [ $stage -le 3 ]; then
    echo -e "\n--> $0: creating neural net configs using the xconfig parser.."
    mkdir -p $dir/configs
    num_targets=$(tree-info $tree_dir/tree | grep num-pdfs | awk '{print $2}')
    learning_rate_factor=$(echo "print(0.5/$xent_regularize)" | python)

    cat <<EOF > $dir/configs/network.xconfig
    input dim=100 name=ivector
    input dim=40 name=input

    # please note that it is important to have input layer with the name=input
    # as the layer immediately preceding the fixed-affine-layer to enable
    # the use of short notation for the descriptor
    fixed-affine-layer name=lda input=Append(-2,-1,0,1,2,ReplaceIndex(ivector, t, 0)) affine-transform-file=$dir/configs/lda.mat

    # the first splicing is moved before the lda layer, so no splicing here
    relu-renorm-layer name=tdnn1 dim=512
    relu-renorm-layer name=tdnn2 dim=512 input=Append(-1,0,1)
    relu-renorm-layer name=tdnn3 dim=512 input=Append(-1,0,1)
    relu-renorm-layer name=tdnn4 dim=512 input=Append(-3,0,3)
    relu-renorm-layer name=tdnn5 dim=512 input=Append(-3,0,3)
    relu-renorm-layer name=tdnn6 dim=512 input=Append(-6,-3,0)

    ## adding the layers for chain branch
    relu-renorm-layer name=prefinal-chain dim=512 target-rms=0.5
    output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5

    # adding the layers for xent branch
    # This block prints the configs for a separate output that will be
    # trained with a cross-entropy objective in the 'chain' models... this
    # has the effect of regularizing the hidden parts of the model.  we use
    # 0.5 / args.xent_regularize as the learning rate factor- the factor of
    # 0.5 / args.xent_regularize is suitable as it means the xent
    # final-layer learns at a rate independent of the regularization
    # constant; and the 0.5 was tuned so as to make the relative progress
    # similar in the xent and regular final layers.
    relu-renorm-layer name=prefinal-xent input=tdnn6 dim=512 target-rms=0.5
    output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF

    steps/nnet3/xconfig_to_configs.py \
        --xconfig-file $dir/configs/network.xconfig --config-dir $dir/configs
fi

if [ $stage -le 4 ]; then
    echo -e "\n--> $0: start chain training.."
    steps/nnet3/chain/train.py \
        --stage=$train_stage \
        --cmd="$cmd" \
        --feat.online-ivector-dir=$train_ivector_dir \
        --feat.cmvn-opts="--norm-means=false --norm-vars=false" \
        --chain.xent-regularize $xent_regularize \
        --chain.leaky-hmm-coefficient=0.1 \
        --chain.l2-regularize=0.005 \
        --chain.apply-deriv-weights=false \
        --chain.lm-opts="--num-extra-lm-states=2000" \
        --trainer.srand=$srand \
        --trainer.max-param-change=2.0 \
        --trainer.num-epochs=$num_epochs \
        --trainer.frames-per-iter=3000000 \
        --trainer.dropout-schedule=$dropout_schedule \
        --trainer.optimization.num-jobs-initial=$num_gpu \
        --trainer.optimization.num-jobs-final=$num_gpu \
        --trainer.optimization.initial-effective-lrate=0.001 \
        --trainer.optimization.final-effective-lrate=0.0001 \
        --trainer.optimization.shrink-value=1.0 \
        --trainer.optimization.proportional-shrink=60.0 \
        --trainer.num-chunk-per-minibatch=128,256,64 \
        --trainer.optimization.momentum=0.0 \
        --egs.chunk-width=$chunk_width \
        --egs.chunk-left-context=$chunk_left_context \
        --egs.chunk-right-context=$chunk_right_context \
        --egs.chunk-left-context-initial=0 \
        --egs.chunk-right-context-final=0 \
        --egs.opts="--frames-overlap-per-eg 0" \
        --cleanup.remove-egs=$remove_egs \
        --use-gpu=yes \
        --feat-dir=$train_data_dir \
        --tree-dir=$tree_dir \
        --lat-dir=$lat_dir \
        --dir=$dir || exit 1
fi

if [ $stage -le 5 ]; then
    # The reason we are using $lang here, instead of $lang, is just to
    # emphasize that it's not actually important to give mkgraph.sh the
    # lang directory with the matched topology (since it gets the
    # topology file from the model). So you could give it a different
    # lang directory, one that contained a wordlist and LM of your choice,
    # as long as phones.txt was compatible.
    echo -e "\n--> $0: preparing chain graph.."
    utils/lang/check_phones_compatible.sh $lang/phones.txt $lang/phones.txt
    utils/mkgraph.sh --self-loop-scale 1.0 \
        $lang $tree_dir $tree_dir/graph_tg || exit 1
fi

if [ $stage -le 6 ]; then
    echo -e "\n--> $0: start decoding for 'test' and 'dev' splits.."
    frames_per_chunk=$(echo $chunk_width | cut -d, -f1)
    rm $dir/.error 2>/dev/null || true
    for data in test dev; do
        (
            ivectors=$data_fold/ivector/${data}_hires
            steps/nnet3/decode.sh \
                --acwt 1.0 --post-decode-acwt 10.0 \
                --extra-left-context "$chunk_left_context" \
                --extra-right-context "$chunk_right_context" \
                --extra-left-context-initial 0 \
                --extra-right-context-final 0 \
                --frames-per-chunk "$frames_per_chunk" \
                --nj "$nj" --cmd "$cmd" --num-threads 4 \
                --online-ivector-dir "$ivectors" \
                $tree_dir/graph_tg \
                ${data_fold}/split/${data}_hires \
                ${dir}/decode_${data} || exit 1
        ) || touch $dir/.error &
    done
    wait
    [[ -f $dir/.error ]] && \
        echo -e "\n--> $0: there was a problem in chain decoding !" && exit 1
fi

echo -e "\n--> $0: successful on training/decoding chain models"
exit 0
