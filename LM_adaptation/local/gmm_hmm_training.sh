#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

. ./path.sh

set -euo pipefail

# you can increase "nj" based on your CPU usage
nj=4

cmd=run.pl
repo=data
train_data=$repo/split/train
lang=$repo/lang
number_of_leaves=5000
total_gaussians=80000

echo "--> start monophone training over reduced dataset.."
utils/subset_data_dir.sh --shortest \
    $train_data 20000 data/train_20k || exit 1
steps/train_mono.sh --nj "$nj" --cmd "$cmd" \
    data/train_20k $lang exp/mono || exit 1
steps/align_si.sh --nj "$nj" --cmd "$cmd" \
    $train_data $lang exp/mono exp/mono_align || exit 1

echo -e "\n--> context-dependent training with delta features.."
steps/train_deltas.sh --cmd "$cmd" "$number_of_leaves" "$total_gaussians" \
    $train_data $lang exp/mono_align exp/tri1 || exit 1
steps/align_si.sh --nj "$nj" --cmd "$cmd" \
    $train_data $lang exp/tri1 exp/tri1_align || exit 1

echo -e "\n--> train LDA-MLLT triphones and align with fMLLR.."
steps/train_lda_mllt.sh --cmd "$cmd" --splice-opts "--left-context=3 --right-context=3" "$number_of_leaves" "$total_gaussians" \
    $train_data $lang exp/tri1_align exp/tri2 || exit 1
steps/align_fmllr.sh --nj "$nj" --cmd "$cmd" \
    $train_data $lang exp/tri2 exp/tri2_align || exit 1

echo -e "\n--> train SAT triphones and align with fMLLR.."
steps/train_sat.sh --cmd "$cmd" "$number_of_leaves" "$total_gaussians" \
    $train_data $lang exp/tri2_align exp/tri3 || exit 1
steps/align_fmllr.sh --nj "$nj" --cmd "$cmd" \
    $train_data $lang exp/tri3 exp/tri3_align || exit 1

graph=exp/tri3/graph
$cmd $graph/mkgraph.log utils/mkgraph.sh $lang exp/tri3 $graph || exit 1
for decode_set in dev test; do
    (
        steps/decode_fmllr.sh --nj "$nj" --cmd "$cmd" \
            --config conf/decode.conf $graph \
            $repo/split/$decode_set exp/tri3/decode_${decode_set} || exit 1
    ) &
done

echo -e "\n--> resegment train data and select only 'good' samples.."
segmentation_opts="--min-segment-length 0.3 --min-new-segment-length 0.6"
cleaned_data=data/train_cleaned
steps/cleanup/clean_and_segment_data.sh --nj "$nj" --cmd "$cmd" \
    --segmentation-opts "$segmentation_opts" $train_data \
    $lang exp/tri3 exp/tri3_cleaned $cleaned_data || exit 1
steps/align_fmllr.sh --nj "$nj" --cmd "$cmd" \
    $cleaned_data $lang exp/tri3 exp/tri3_align_cleaned || exit 1
steps/train_sat.sh --cmd "$cmd" "$number_of_leaves" "$total_gaussians" \
    $cleaned_data $lang exp/tri3_align_cleaned exp/tri3_cleaned || exit 1

graph=exp/tri3_cleaned/graph
$cmd $graph/mkgraph.log utils/mkgraph.sh $lang exp/tri3_cleaned $graph ||exit 1

rm -f exp/.error || true
for decode_set in dev test; do
    (
        steps/decode_fmllr.sh --nj "$nj" --cmd "$cmd" \
            --config conf/decode.conf $graph $repo/split/$decode_set \
            exp/tri3_cleaned/decode_${decode_set} || exit 1
    )
done

echo -e "\n--> $0: successful on training tri3 models"
exit 0
