#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

set -euo pipefail

echo "--> $0 $@"

output_folder=data/wclass/resources

. ./utils/parse_options.sh

mkdir -p $output_folder

cat << EOT | tr -s " " "\t" >> $output_folder/wclass_list.txt
C=ORG  0.0
C=PER  0.0
C=LOC  0.0
C=MISC 0.0
EOT

prefix='C='

cat << EOT >> $output_folder/must_deleted.list
_
THERE
THERE'RE
YEP
AND
THEY
THEY'RE
EVERYTHING
INTERFACE
WE'RE
WE'LL
WE'D
MM
UH
HMM
THE
I'M
I'VE
I'LL
IT'S
IT'LL
EOT

cat << EOT >> $output_folder/spacy.map
PER PERSON
LOC FAC
LOC GPE
LOC LOC
ORG ORG
MISC DATE
MISC TIME
MISC MONEY
EOT

echo "--> $0: extracting entities using spacy 'en_core_web_sm-2.3.1' package.."
cut -f2- -d' ' data/split/train/text | python3 local/spacy_ner.py | \
    grep -f <(awk '{print $2}' $output_folder/spacy.map) \
    > $output_folder/extracted_spacy.ner

touch $output_folder/ner_spacy
for ner in $(awk '{print $2}' $output_folder/spacy.map); do
    wclass=$(grep "$ner$" $output_folder/spacy.map | cut -f1 -d ' ')
    matched=$(grep "^$ner" $output_folder/extracted_spacy.ner | cut -d" " -f2-)

    count=$(echo "$matched" | sort -u | wc -l)
    echo " - $count total SPACY items for $wclass:$ner"

    echo "$matched" | sort -u | while IFS='' read -r line; do
        read_entity=true
        for word in $line; do
            [[ ${#word} -le 2 ]] && read_entity=false && break
        done
        if ! echo "$line" | grep -q -f $output_folder/must_deleted.list; then
            if $read_entity; then
                echo "$prefix$wclass $line" >> $output_folder/ner_spacy
            fi
        fi
    done
done
sed -i "/'/d" $output_folder/ner_spacy
awk '{print $1}' $output_folder/ner_spacy | sort | uniq -c && echo ""

cat << EOT >> $output_folder/ami.map
PER PROJECT_MANAGER
PER INTERFACE_SPECIALIST
PER MARKETING
PER INDUSTRIAL_DESIGNER
PER EXPERIMENTER
PER OTHER
LOC LOCATION
ORG ORGANIZATION
MISC TIME
MISC DATE
MISC DURATION
EOT

touch $output_folder/ner_ami
for ner in $(awk '{print $2}' $output_folder/ami.map); do
    wclass=$(grep "$ner$" $output_folder/ami.map | cut -f1 -d ' ')
    matched=$(grep "$ner" data/ami_ner/query/ner.log | column -t | tr -s " " |\
              cut -d" " -f 3- | awk 'NF{NF-=1};1' | tr '[:lower:]' '[:upper:]')

    count=$(echo "$matched" | sort -u | wc -l)
    echo " - $count total AMI items for $wclass:$ner"
    echo "$matched" | sort -u | while IFS='' read -r line; do
        read_entity=true
        for word in $line; do
            [[ ${#word} -le 2 ]] && read_entity=false && break
        done
        if ! echo "$line" | grep -q -f $output_folder/must_deleted.list; then
            if $read_entity; then
                echo "$prefix$wclass $line" >> $output_folder/ner_ami
            fi
        fi
    done
done
sed -i "/'/d" $output_folder/ner_ami
awk '{print $1}' $output_folder/ner_ami | sort | uniq -c && echo ""

cat $output_folder/ner_* | sort -u > $output_folder/ner.classes

col1=$(cat $output_folder/ner.classes | cut -f2- -d' ' | \
       sed 's/ /-/g' | grep "-")
col2=$(cat $output_folder/ner.classes | cut -f2- -d' ' | \
       sed 's/ /-/g' | grep "-" | sed 's/-/ /g' | python3 local/g2p_oov.py)

i=1
touch $output_folder/extra.lex
echo "$col2" | while IFS='' read -r line; do
    del=$(echo "$col1" | sed 's/-/ /g' | sed "${i}q;d" || true)
    ret=$({ echo "$del" | sed 's/ /-/g'; \
            echo ${line#${del}} | sed ''s:\'::g''; } | tr "\n" " ")
    echo "$ret" | column -t | tr -s " " >> $output_folder/extra.lex
    ((i++))
done

cp -r data/local/dict $output_folder/../
mv $output_folder/../dict/lexicon.txt $output_folder/../dict/lexicon_nocls.txt
cat $output_folder/../dict/lexicon_nocls.txt $output_folder/extra.lex | \
    sort -u > $output_folder/../dict/lexicon.txt
rm $output_folder/../dict/lexiconp.txt

echo "--> $0: 'data/wclass/resources' folder is successfully created"
exit 0
