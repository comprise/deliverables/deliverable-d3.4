#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

. ./cmd.sh
. ./path.sh

set -euo pipefail

echo "==> running $0 $@.."

# locate your AMI dataset folders
# to downlaod and process annotations, please refer to "local/download_text.sh"
#
AMI=/your/speech/samples
AMI_TEXT=/your/data/annotations
# -------------------------------

mkdir -p data/local
cp -r $AMI_TEXT data/local/
cp LM_adaptation/misc/meeting_ids/*.sessions data/local

# find headset wav audio files only
flist=data/local/wav_file.list
find $AMI -iname '*.Headset-*.wav' | sort > $flist

for split in train test dev; do
    ami_text=data/local/annotations/$split.txt
    dir=data/local/split/$split
    mkdir -p $dir
    odir=data/ami/$split
    mkdir -p $odir

    # (1) prepare transcriptions
    # start with normalised transcriptions, utt-IDs follow this convention:
    # AMI_MEETING_CHANNEL_SPEAKER_STARTTIME_ENDTIME
    # AMI_ES2011a_H00_FEE041_0003415_0003484
    awk '{
            meet=$1; chn=$2; spk=$3; stime=$4; etime=$5;
            printf("AMI_%s_%s_%s_%07.0f_%07.0f", meet, chn, spk, int(100*stime+0.5), int(100*etime+0.5));
            for(i=6;i<=NF;i++) printf(" %s", $i); printf "\n"
        }' ${ami_text} | sort | uniq > $dir/text

    # (2) make segment file
    awk '{
            segment=$1;
            split(segment,S,"[_]");
            audioname=S[1]"_"S[2]"_"S[3]; startf=S[5]; endf=S[6];
            print segment " " audioname " " startf*10/1000 " " endf*10/1000 " "
        }' < $dir/text > $dir/segments

    # (3) make ".scp" file
    sed -e 's?.*/??' -e 's?.wav??' $flist | \
        perl -ne 'split; $_ =~ m/(.*)\..*\-([0-9])/; print "AMI_$1_H0$2\n"' | \
        paste - $flist > $dir/wav1.scp

    # (4) keep only the train part of waves
    awk '{print $2}' $dir/segments | sort -u | join - $dir/wav1.scp \
        > $dir/wav2.scp
    echo "- total $split duration: $(cat $dir/wav2.scp | \
        awk '{print $2}' | xargs soxi -D | \
        awk '{S += $1} END {printf "%d:%d:%d\n",S/3600,S%3600/60,S%60}')"

    # (5) replace path with a "sox" command that only selects single channel
    awk '{print $1" sox -c 1 -t wavpcm -e signed-integer "$2" -t wavpcm - |"}' $dir/wav2.scp > $dir/wav.scp

    # (6) prepare "reco2file_and_channel" for mapping rules ("glm" and "stm")
    cat $dir/wav.scp | \
        perl -ane '$_ =~ m:^(\S+)(H0[0-4])\s+.*\/([IETB].*)\.wav.*$: || die "bad label $_";
            print "$1$2 $3 A\n"; ' > $dir/reco2file_and_channel || exit 1

    # (7) make "utt2spk" by joining underscore-separated fields of utterance-ID
    awk '{print $1}' $dir/segments | \
        perl -ane '$_ =~ m:^(\S+)([FM][A-Z]{0,2}[0-9]{3}[A-Z]*)(\S+)$: || die "segments: bad label $_"; print "$1$2$3 $1$2\n";' \
        > $dir/utt2spk || exit 1

    # (8) make "spk2utt" using a default kaldi script
    utils/utt2spk_to_spk2utt.pl < $dir/utt2spk > $dir/spk2utt || exit 1

    # correct case when segment timings for given speaker overlap themself
    join $dir/utt2spk $dir/segments | \
        perl -ne '{
           BEGIN{$pu=""; $pt=0.0;} split;
           if ($pu eq $_[1] && $pt > $_[3])
           {print "s/^$_[0] $_[2] $_[3] $_[4]\$/$_[0] $_[2] $pt $_[4]/;\n"}
           $pu=$_[1]; $pt=$_[4];
        }' > $dir/segments_to_fix

    if [ -s $dir/segments_to_fix ]; then
        echo "$0: applying following fixes to segments for $1"
        cat $dir/segments_to_fix
        perl -i -pf $dir/segments_to_fix $dir/segments
    fi

    glm=github.com/kaldi-asr/kaldi/raw/master/egs/ami/s5/local/english.glm
    wget -nv -q -O $odir/glm $glm
    local/convert2stm.pl $dir > $odir/stm

    # (9) copy them into its final location
    for f in spk2utt utt2spk wav.scp text segments reco2file_and_channel; do
        cp $dir/$f $odir/$f || exit 1
    done

    utils/validate_data_dir.sh --no-feats $odir || exit 1
done

echo -e "\n==> $0: finished preparing train/test/adaptation splits"
exit 0
