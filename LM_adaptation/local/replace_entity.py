#!/bin/python
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import sys
import copy
import numpy as np
import pandas as pd

wclass_list = sys.argv[1]
ner_classes = sys.argv[2]

# increasing number of duplicates gives negative modified KneserNey discounts
# https://moses-support.mit.narkive.com/ccY4Ljvx/one-of-modified-kneserney-discounts-is-negative
repeat = 1

# ['C=ORG', 'C=PER', 'C=LOC', 'C=MISC']
classes = np.loadtxt(wclass_list, dtype=str, usecols=0).tolist()
skip_list = [100, 50, 10, 50]
skip_amount = {key: skip_list[i] for i, key in enumerate(classes)}

with open(ner_classes, 'r') as f:
    class_file = f.read().splitlines()

ner_type = [line.split()[0] for line in class_file]
ner_count = [line.split()[1] for line in class_file]
ner_list = [" ".join(line.split()[2:]) for line in class_file]

all_ner = pd.DataFrame({'type': ner_type, 'count': ner_count, 'ner': ner_list})
all_ner = all_ner.convert_dtypes().astype({"count": int})

for text in sys.stdin:
    sentence = text.split()
    ner_dict = {key: None for key in classes}
    for key in ner_dict:
        ner_dict[key] = [i for i, x in enumerate(text.split()) if x == key]
    non_empty_values = {k: v for k, v in copy.deepcopy(ner_dict).items() if v}
    if non_empty_values:
        mesh_list = []
        repeat_dict = {key: [] for key in copy.deepcopy(non_empty_values)}
        for item in non_empty_values:
            matched = all_ner[all_ner['type'].str.contains(item)]
            tmp_df = matched.sort_values(by=['count'], ascending=False)
            skipped = tmp_df[skip_amount[item]:]
            probs = skipped['count'] / np.sum(skipped['count'])
            for _ in range(len(ner_dict[item])):
                random_ners = np.random.choice(skipped['ner'], repeat, p=probs)
                repeat_dict[item].append(random_ners)
                mesh_list.append(list(range(1, repeat + 1)))
            # mesh_list.append(list(range(1, ((i+1)*repeat)+1)))
        shaping = len(np.concatenate(list(non_empty_values.values())))
        permutations = np.array(np.meshgrid(*mesh_list)).T.reshape(-1, shaping)
        whole_words = np.concatenate(list(repeat_dict.values()))
        whole_index = np.concatenate(list(non_empty_values.values()))
        for itm in permutations:
            out_sentence = sentence
            for i in range(len(whole_index)):
                out_sentence[whole_index[i]] = whole_words[i][itm[i] - 1]
            print(" ".join(out_sentence))
    else:
        print(" ".join(sentence))
