#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

if [ $# -ne 1 ]; then
    echo "Usage: $0 <ami-dir>"
    exit 1
fi

adir=$1
wdir=data/local/annotations

[ ! -f $adir/annotations/AMI-metadata.xml ] && \
    echo "$0: File $adir/annotations/AMI-metadata.xml no found." && exit 1

mkdir -p $wdir/log

if [ ! -d $wdir/nxt ]; then
    echo "Downloading NXT annotation tool..."
    wget -O $wdir/nxt.zip \
        http://sourceforge.net/projects/nite/files/nite/nxt_1.4.4/nxt_1.4.4.zip
    [ ! -s $wdir/nxt.zip ] && echo "Download Failed! ($wdir/nxt.zip)" && exit 1
    unzip -d $wdir/nxt $wdir/nxt.zip &> /dev/null
fi

if [ ! -f $wdir/transcripts0 ]; then
    echo "Parsing XML files (can take several minutes)..."
    nxtlib=$wdir/nxt/lib
    java -cp $nxtlib/nxt.jar:$nxtlib/xmlParserAPIs.jar:$nxtlib/xalan.jar:$nxtlib \
        FunctionQuery -c $adir/annotations/AMI-metadata.xml -q '($s segment)(exists $w1 w):$s^$w1' -atts obs who \
        '@extract(($sp speaker)($m meeting):$m@observation=$s@obs && $m^$sp & $s@who==$sp@nxt_agent,global_name, 0)' \
        '@extract(($sp speaker)($m meeting):$m@observation=$s@obs && $m^$sp & $s@who==$sp@nxt_agent, channel, 0)' \
        transcriber_start transcriber_end starttime endtime '$s' '@extract(($w w):$s^$w & $w@punc="true", starttime,0,0)' \
        1> $wdir/transcripts0 2> $wdir/log/nxt_export.log
fi

#remove NXT logs dumped to stdio
grep -e '^Found' -e '^Obs' -i -v $wdir/transcripts0 > $wdir/transcripts1

exit 0
