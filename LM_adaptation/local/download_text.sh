#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

. ./path.sh

# Set bash to 'debug' mode, it prints the commands (option '-x') and exits on
# -e 'error', -u 'undefined variable', -o pipefail 'error in pipeline'
set -euxo pipefail

dir=data/local/downloads
mkdir -p $dir

amiurl=http://groups.inf.ed.ac.uk/ami
annotver=ami_public_manual_1.6.2
annot="$dir/$annotver"

logdir=data/local/downloads
mkdir -p $logdir/log
[ ! -f $annot.zip ] && wget -nv -O $annot.zip $amiurl/AMICorpusAnnotations/$annotver.zip &> $logdir/log/download_ami_annot.log

if [ ! -d $dir/annotations ]; then
    mkdir -p $dir/annotations
    unzip -o -d $dir/annotations $annot.zip &> /dev/null
fi

[ ! -f "$dir/annotations/AMI-metadata.xml" ] && echo "$0: File AMI-Metadata.xml not found under $dir/annotations." && exit 1

# extract text from AMI XML annotations
local/ami_xml2text.sh $dir

wdir=data/local/annotations
[ ! -f $wdir/transcripts1 ] && echo "$0: File $wdir/transcripts1 not found." && exit 1

echo "Preprocessing transcripts..."
local/ami_split_segments.pl $wdir/transcripts1 $wdir/transcripts2 &> $wdir/log/split_segments.log

# make final train/dev/eval splits
for dset in train test dev; do
    grep -f local/split_$dset.orig $wdir/transcripts2 > $wdir/$dset.txt
done

echo -e "\n==> $0: AMI corpus was downloaded successfully!"
exit 0
