#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

. ./path.sh
. ./cmd.sh

# your main data folder
data_folder=/path/to/your/data/folder

# adjust number of jobs according to your cpu capability
nj=4

echo -e "**-->(1/4) $0: prepare data/local/lang and data/lang \n"

# required files for data/local/lang and data/lang:
# put {extra_questions,nonsilence_phones,optional_silence,silence_phones}.txt
# and combined lexicon.txt under "${data_folder}/local/dict"

utils/prepare_lang.sh data/local/dict "<unk>" data/local/lang data/lang

echo -e "\n**-->(2/4) $0: prepare 'word_map' and 'train.gz' for 'kaldi-lm' \n"

# put "train" and "test" files under "${data_folder}/split" folder
text=${data_folder}/split/train/text
lexicon=${data_folder}/local/dict/lexicon.txt

test_lm=data/lang_test
mkdir -p $test_lm/local

awk '{print $1}' $lexicon | grep -v -w '!sil' > $test_lm/local/word_list.txt

cut -f 2- -d ' ' $text | tr '[:upper:]' '[:lower:]' > $test_lm/local/all.trans

cat $test_lm/local/all.trans | awk -v w=$test_lm/local/word_list.txt \
    'BEGIN{while((getline<w)>0) v[$1]=1;}
    {for (i=1;i<=NF;i++) if ($i in v) printf $i" ";else printf "<unk> ";print ""}' | \
    sed 's/ $//g' | gzip -c > $test_lm/local/train_nounk.gz

gunzip -c $test_lm/local/train_nounk.gz | cat - $test_lm/local/word_list.txt |\
    awk '{ for(x=1;x<=NF;x++) count[$x]++; } END{for(w in count){print count[w], w;}}' | \
    sort -nr > $test_lm/local/unigram.counts

cat $test_lm/local/unigram.counts | awk '{print $2}' | \
    get_word_map.pl "<s>" "</s>" "<unk>" > $test_lm/word_map

gunzip -c $test_lm/local/train_nounk.gz | \
    awk -v wmap=$test_lm/word_map 'BEGIN{while((getline<wmap)>0)map[$1]=$2;}
    { for(n=1;n<=NF;n++) { printf map[$n]; if(n<NF){ printf " "; } else { print ""; } } }' | gzip -c > $test_lm/train.gz

echo -e "\n**-->(3/4) $0: train kaldi language model\n"

train_lm.sh --arpa --lmtype 3gram $test_lm

cp -r data/lang/* $test_lm/

gunzip -c $test_lm/3gram/lm_unpruned.gz | \
    arpa2fst --disambig-symbol=#0 \
        --read-symbol-table=$test_lm/words.txt - $test_lm/G.fst

fstisstochastic $test_lm/G.fst

echo -e "\n**-->(4/4) $0: MFCC feature extration for train and test sets\n"
steps/make_mfcc.sh --cmd "$train_cmd" --nj $nj data/split/train || exit 1
steps/compute_cmvn_stats.sh data/split/train || exit 1

steps/make_mfcc.sh --cmd "$train_cmd" --nj $nj data/split/test || exit 1
steps/compute_cmvn_stats.sh data/split/test || exit 1

echo -e "\n$0: successfully performed data preparation.."
exit 0
