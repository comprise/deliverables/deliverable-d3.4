#!/usr/bin/python
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import sys
import glob
import numpy
import kaldiio
import argparse
from tqdm import tqdm

parser = argparse.ArgumentParser(description='Merge x-vectors into one ark')

parser.add_argument('folder', type=str, help='Data folder, train|dev|test')
parser.add_argument('xdir', type=str, help='Input XVector folder')
parser.add_argument('xfold', type=str, help='Output XVector folder')

args = parser.parse_args()

utts = []
xvecs = []

print("\n", sys.argv[0], ": concatenation of arks from", args.xdir, "...")

scpList = glob.glob(args.xdir + '/*.*.scp')
for s in tqdm(scpList):
    scp = kaldiio.load_scp(s)
    utt = list(scp._dict.keys())
    uttUnique = list(set([u.rpartition('_')[0] for u in utt]))
    for line in uttUnique:
        match = [s for s in utt if line in s]
        merge = []
        for u in match:
            tmp = scp.get(u)
            merge.append(tmp)
        utts.append(line)
        xvecs.append(numpy.asarray(merge))

indList = []
with open(args.folder + '/utt2spk') as uttFile:
    print(sys.argv[0],": writing arks to", args.folder, "...")
    for line in uttFile:
        uttName = line.split()[0]
        ind = utts.index(uttName)
        indList.append(ind)

out = 'ark,scp:' + \
    args.xfold + '/xvector.ark,' + args.xfold + '/ivector_online.scp'

matchedNames = list(map(utts.__getitem__, indList))
matchedXvecs = list(map(xvecs.__getitem__, indList))
with kaldiio.WriteHelper(out, compression_method=1) as writer:
    for i in range(len(matchedNames)):
        writer[str(matchedNames[i])] = matchedXvecs[i]

print(sys.argv[0],": x-vectors were merged")
