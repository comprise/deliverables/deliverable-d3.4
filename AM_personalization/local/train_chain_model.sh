#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

set -e -o pipefail

. ./cmd.sh
. ./path.sh

nnet3_affix=
echo "$0 $@"

# -------------------- START CONFIGURATION -------------------- #
# please use a gpu-machine to train acoustic model
# don't forget to set compute mode if you use multiple-gpu
# nvidia-smi -c 3
nj=number-of-jobs
train_data_dir=path/to/hi-res/train_sp/folder     (40-dim and speed-perturbed)
lores_train_data=path/to/low-res/train_sp/folder  (13-dim and speed-perturbed)
gmm_dir=path/to/tri_phone_gmm/folder              (e.g. exp/tri3)
ali_dir=path/to/tri_phone/train/alignments        (e.g. exp/tri3_ali_train_sp)
main_lang_folder=path/to/language_files           (e.g. data/lang)
# --------------------- END CONFIGURATION --------------------- #

stage=0
num_threads_ubm=32
lang=exp/chain${nnet3_affix}/lang
dir=exp/chain${nnet3_affix}/tdnn_sp
tree_dir=exp/chain${nnet3_affix}/tree_sp
lat_dir=exp/chain${nnet3_affix}/train_sp_lats
xvector_dir=exp/xvectors/${nnet3_affix}/train

for f in $train_data_dir/feats.scp $lores_train_data/feats.scp \
    $gmm_dir/final.mdl $ali_dir/ali.1.gz $xvector_dir/ivector_online.scp; do
    [ ! -f $f ] && echo "$0: expected file $f to exist" && exit 1
done

# LSTM/chain options
train_stage=-10
xent_regularize=0.1

# Training chunk-options
chunk_width=140,100,160

# Need extra for left/right context in TDNN systems
chunk_left_context=0
chunk_right_context=0

# training options
srand=123
remove_egs=true

if [ $stage -le 0 ]; then
    echo "$0: creating lang directory $lang with chain-type topology.."
    # create a version of the 'lang' that has one state per phone in the topo
    # file. [note, it really has two states.. the first one is only repeated
    # once, the second one has zero or more repeats.]

    if [ -d $lang ]; then
        if [ $lang/L.fst -nt $main_lang_folder/L.fst ]; then
            echo "$0: $lang already exists, not overwriting it; continuing"
        else
            echo "$0: $lang seems to be older than $main_lang_folder..."
            echo " ... not sure what to do. Exiting!"
            exit 1
        fi
    else
        cp -r $main_lang_folder $lang
        silphonelist=$(cat $lang/phones/silence.csl) || exit 1
        nonsilphonelist=$(cat $lang/phones/nonsilence.csl) || exit 1

        # use our special topology... note that may have to tune this topology
        steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist \
            > $lang/topo
    fi
fi

if [ $stage -le 1 ]; then
    # get the alignments as lattices (gives the chain training more freedom)
    # use the same num-jobs as the alignments
    steps/align_fmllr_lats.sh --nj "$nj" --cmd "$train_cmd" \
        ${lores_train_data} $lang $gmm_dir $lat_dir
    rm $lat_dir/fsts.*.gz
fi

if [ $stage -le 2 ]; then
    # Build a tree using our new topology.  We know we have alignments for the
    # speed-perturbed data (local/train_ivector.sh made them)
    # The num-leaves is always somewhat less than the num-leaves from the GMM.

    if [ -f $tree_dir/final.mdl ]; then
        echo "$0: $tree_dir/final.mdl already exists, refusing to overwrite!"
        exit 1
    fi

    steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
        --context-opts "--context-width=2 --central-position=1" \
        --cmd "$train_cmd" 3500 ${lores_train_data} $lang $ali_dir $tree_dir
fi

if [ $stage -le 3 ]; then
    mkdir -p $dir
    echo "$0: creating neural net configs using the xconfig parser.."

    num_targets=$(tree-info $tree_dir/tree | grep num-pdfs | awk '{print $2}')
    learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python2)

    mkdir -p $dir/configs

    cat << EOF > $dir/configs/network.xconfig
    input dim=512 name=ivector
    input dim=40 name=input

    # please note that it is important to have input layer with the name=input
    # as the layer immediately preceding the fixed-affine-layer to enable
    # the use of short notation for the descriptor
    fixed-affine-layer name=lda input=Append(-2,-1,0,1,2,ReplaceIndex(ivector, t, 0)) affine-transform-file=$dir/configs/lda.mat

    # the first splicing is moved before the lda layer, so no splicing here
    relu-renorm-layer name=tdnn1 dim=512
    relu-renorm-layer name=tdnn2 dim=512 input=Append(-1,0,1)
    relu-renorm-layer name=tdnn3 dim=512 input=Append(-1,0,1)
    relu-renorm-layer name=tdnn4 dim=512 input=Append(-3,0,3)
    relu-renorm-layer name=tdnn5 dim=512 input=Append(-3,0,3)
    relu-renorm-layer name=tdnn6 dim=512 input=Append(-6,-3,0)

    ## adding the layers for chain branch
    relu-renorm-layer name=prefinal-chain dim=512 target-rms=0.5
    output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5

    # adding the layers for xent branch
    # This block prints the configs for a separate output that will be
    # trained with a cross-entropy objective in the 'chain' models... this
    # has the effect of regularizing the hidden parts of the model.  we use
    # 0.5 / args.xent_regularize as the learning rate factor- the factor of
    # 0.5 / args.xent_regularize is suitable as it means the xent
    # final-layer learns at a rate independent of the regularization
    # constant; and the 0.5 was tuned so as to make the relative progress
    # similar in the xent and regular final layers.
    relu-renorm-layer name=prefinal-xent input=tdnn6 dim=512 target-rms=0.5
    output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF

    steps/nnet3/xconfig_to_configs.py \
        --xconfig-file $dir/configs/network.xconfig --config-dir $dir/configs
fi

if [ $stage -le 4 ]; then
    steps/nnet3/chain/train.py \
        --stage=$train_stage \
        --cmd="$decode_cmd" \
        --feat.online-ivector-dir=$xvector_dir \
        --feat.cmvn-opts="--norm-means=false --norm-vars=false" \
        --chain.xent-regularize $xent_regularize \
        --chain.leaky-hmm-coefficient=0.1 \
        --chain.l2-regularize=0.00005 \
        --chain.apply-deriv-weights=false \
        --chain.lm-opts="--num-extra-lm-states=2000" \
        --trainer.srand=$srand \
        --trainer.max-param-change=2.0 \
        --trainer.num-epochs=4 \
        --trainer.frames-per-iter=3000000 \
        --trainer.optimization.num-jobs-initial=1 \
        --trainer.optimization.num-jobs-final=2 \
        --trainer.optimization.initial-effective-lrate=0.001 \
        --trainer.optimization.final-effective-lrate=0.0001 \
        --trainer.optimization.shrink-value=1.0 \
        --trainer.optimization.proportional-shrink=60.0 \
        --trainer.num-chunk-per-minibatch=128,256,64 \
        --trainer.optimization.momentum=0.0 \
        --egs.chunk-width=$chunk_width \
        --egs.chunk-left-context=$chunk_left_context \
        --egs.chunk-right-context=$chunk_right_context \
        --egs.chunk-left-context-initial=0 \
        --egs.chunk-right-context-final=0 \
        --egs.opts="--frames-overlap-per-eg 0" \
        --cleanup.remove-egs=$remove_egs \
        --use-gpu=yes \
        --feat-dir=$train_data_dir \
        --tree-dir=$tree_dir \
        --lat-dir=$lat_dir \
        --dir=$dir || exit 1
fi

echo -e "\n$0: successfully done acoustic model training.."
exit 0
