#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

set -e -o pipefail

. ./cmd.sh
. ./path.sh

nnet3_affix=
echo "$0 $@"

# -------------------- START CONFIGURATION -------------------- #
nj=number-of-jobs
test_data_dir=path/to/hi-res/test/folder   (40-dim)
openfst_path=path/to/recently/installed/openfst/with/lookahead/option
# --------------------- END CONFIGURATION --------------------- #

if [ ! -f "${openfst_path}/lib/libfstlookahead.so" ]; then
    echo "missing ${openfst_path}/lib/libfstlookahead.so"
    echo "make sure that you compiled OpenFST with lookahead support"
    echo "check 'README_OpenFST.txt' to get more information"
    exit 1
fi

if ! command -v ngramread &> /dev/null; then
    echo "ngramread could not be found"
    echo "you appear to not have OpenGRM tools installed"
    echo "cd to $KALDI_ROOT/tools and run extras/install_opengrm.sh"
    exit 1
fi

export LD_LIBRARY_PATH=${openfst_path}/lib/fst
lang=exp/chain${nnet3_affix}/lang
acs_model=exp/chain${nnet3_affix}/tdnn_sp
xvector_dir=exp/xvectors/${nnet3_affix}/test

utils/mkgraph_lookahead.sh --self-loop-scale 1.0 --remove-oov --compose-graph \
    ${lang} ${acs_model} ${acs_model}/graph_lookahead

steps/nnet3/decode_lookahead.sh \
    --acwt 1.0 --post-decode-acwt 10.0 \
    --nj "$nj" --cmd "$decode_cmd"  --num-threads 4 \
    --online-ivector-dir ${xvector_dir} \
    ${acs_model}/graph_lookahead ${test_data_dir} ${acs_model}/decode || exit 1

echo -e "\n$0: successfully decoded.."
exit 0
