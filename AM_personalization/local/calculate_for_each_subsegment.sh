#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# The script assumes that the x-vector DNN has already been trained, and
# a data directory that contains a segments file and features for the
# x-vector DNN exists. The segments file was most likely created by a
# speech activity detection system that identified the speech segments in
# the recordings. This script performs a subsegmentation, that further
# splits the speech segments into very short non-overlapping subsegments (like
# 0.50 seconds). Finally, x-vectors are extracted for each of the subsegments.

nj=4
cmd="run.pl"
chunk_size=-1     # The chunk size over which the embedding is extracted.
                  # If left unspecified, it uses the max_chunk_size in the nnet

stage=0

apply_cmn=false   # Apply sliding window cepstral mean normalization
use_gpu=false
pca_dim=

echo "$0 $@"      # Print the command line for logging

if [ -f path.sh ]; then . ./path.sh; fi
. ./utils/parse_options.sh || exit 1;

if [ $# != 3 ]; then
    echo "Usage: $0 <nnet-dir> <data> <xvector-splitdir>"
    echo " e.g.: $0 voxceleb/exp/xvector_nnet_1a data/train xvectors/train"
    echo "main options:"
    echo "  --cmd (utils/run.pl|utils/queue.pl <queue opts>) # how to run jobs"
    echo "  --pca-dim <n|-1>                                 # if provided, the whitening transform is done"
    echo "  --chunk-size <n|-1>                              # if provided, extracts embeddings with specified size"
    echo "  --apply-cmn <true,false|true>                    # if true, apply sliding window cepstral mean"
    echo "  --nj <n|20>                                      # number of jobs"
    echo "  --stage <stage|0>                                # control partial reruns"
    exit 1;
fi

srcdir=$1
data=$2
dir=$3

for f in $srcdir/final.raw $srcdir/min_chunk_size $srcdir/max_chunk_size $data/feats.scp $data/utt2dur; do
    [ ! -f $f ] && echo "No such file $f" && exit 1;
done

min_chunk_size=`cat $srcdir/min_chunk_size 2>/dev/null`
max_chunk_size=`cat $srcdir/max_chunk_size 2>/dev/null`

nnet=$srcdir/final.raw
if [ -f $srcdir/extract.config ] ; then
    echo "$0: using $srcdir/extract.config to extract xvectors"
    nnet="nnet3-copy --nnet-config=$srcdir/extract.config $srcdir/final.raw - |"
fi

if [ $chunk_size -le 0 ]; then
    chunk_size=$max_chunk_size
fi

if [ $max_chunk_size -lt $chunk_size ]; then
    echo "$0: specified chunk size of $chunk_size is larger than the maximum chunk size, $max_chunk_size" && exit 1;
fi

sub_data=$dir/subsegments_data
mkdir -p $sub_data

# Set up incremental-window subsegments
if [ $stage -le 0 ]; then
    bash local/get_subsegments.sh $data
    utils/data/subsegment_data_dir.sh $data $data/subsegments $sub_data
fi

# Set up various variables
mkdir -p $dir/log
sub_sdata=$sub_data/split$nj;
utils/split_data.sh $sub_data $nj || exit 1;

# Set up features
if $apply_cmn; then
    feats="ark,s,cs:apply-cmvn-sliding --norm-vars=false --center=true --cmn-window=250 scp:${sub_sdata}/JOB/feats.scp ark:- |"
else
    feats="scp:${sub_sdata}/JOB/feats.scp"
fi

if [ $stage -le 1 ]; then
    echo "$0: extracting xvectors from nnet ..."
    if $use_gpu; then
        for g in $(seq $nj); do
            $cmd --gpu 1 ${dir}/log/extract.$g.log \
                nnet3-xvector-compute --use-gpu=yes \
                --min-chunk-size=$min_chunk_size --chunk-size=$chunk_size \
                "$nnet" "`echo $feats | sed s/JOB/$g/g`" \
                ark,scp:${dir}/xvector.$g.ark,${dir}/xvector.$g.scp || exit 1 &
        done
        wait
    else
        $cmd JOB=1:$nj ${dir}/log/extract.JOB.log \
            nnet3-xvector-compute --use-gpu=no \
            --min-chunk-size=$min_chunk_size --chunk-size=$chunk_size \
            "$nnet" "$feats" \
            ark,scp:${dir}/xvector.JOB.ark,${dir}/xvector.JOB.scp || exit 1;
    fi
fi

if [ $stage -le 2 ]; then
    echo "$0: combining xvectors across jobs"
    for j in $(seq $nj); do cat $dir/xvector.$j.scp; done > $dir/xvector.scp ||exit 1;
    cp $sub_data/{segments,spk2utt,utt2spk} $dir
fi
