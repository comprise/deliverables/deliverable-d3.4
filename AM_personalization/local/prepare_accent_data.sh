#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# your main training data folder with extracted high-resolution MFCCs
# using "conf/mfcc_hires.conf" configuration file
train_data=/path/to/your/train/data/folder

# adjust number of jobs according to your cpu capability
nj=4

min_len=400
data_fold=xvec_data/data_no_sil_speaker
acct_fold=xvec_data/data_no_sil_accent

# create VAD decisions for the training split
steps/compute_vad_decision.sh --nj $nj --cmd "$train_cmd" ${train_data}
utils/fix_data_dir.sh ${train_data}

# apply mean-normalization and VAD
local/apply_CMVN_VAD.sh --nj $nj --cmd "$train_cmd" \
    ${train_data} $data_fold $data_fold/xvector_features_no_sil
mv $data_fold/utt2num_frames $data_fold/utt2num_frames.bak
awk -v min_len=${min_len} '$2 > min_len {print $1, $2}' \
    $data_fold/utt2num_frames.bak > $data_fold/utt2num_frames
utils/filter_scp.pl $data_fold/utt2num_frames $data_fold/utt2spk \
    > $data_fold/utt2spk.new
mv $data_fold/utt2spk.new $data_fold/utt2spk
utils/fix_data_dir.sh $data_fold

utils/copy_data_dir.sh --spk-prefix "accent-" $data_fold $acct_fold
while read line; do
    spk=$(echo "$line" | cut -f1 -d" ")
    class=$(echo "$line" | cut -f2 -d" ")
    sed -i "s/\<accent-$spk\>/accent-$class/g" $$acct_fold/utt2spk
done < $spk2accent

utils/utt2spk_to_spk2utt.pl < $$acct_fold/utt2spk > $$acct_fold/spk2utt

local/apply_CMVN_VAD.sh --nj $nj --cmd "$train_cmd" \
    LSRC_data $$acct_fold $$acct_fold/xvector_features_no_sil
mv $$acct_fold/utt2num_frames $$acct_fold/utt2num_frames.bak
awk -v min_len=${min_len} '$2 > min_len {print $1, $2}' \
    $$acct_fold/utt2num_frames.bak > $$acct_fold/utt2num_frames
utils/filter_scp.pl $$acct_fold/utt2num_frames $$acct_fold/utt2spk \
    > $$acct_fold/utt2spk.new
mv $$acct_fold/utt2spk.new $$acct_fold/utt2spk
utils/fix_data_dir.sh $$acct_fold

echo -e "\n$0: successfully prepared xvector speaker-id data.."
exit 0
