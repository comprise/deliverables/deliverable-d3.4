#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

. ./cmd.sh

# -------------------- START CONFIGURATION -------------------- #
nj=number-of-jobs
train_set=path/to/hi-res/train_sp/folder    (40-dim and speed-perturbed)
test_set=path/to/hi-res/test/folder         (40-dim)
# --------------------- END CONFIGURATION --------------------- #

# output folder for x-vector embeddings
out_fold=exp/xvectors
xvec_fold=data/xvectors
nnet_dir=exp/nnet_xvector

echo -e "$0: start x-vector extraction for train and test splits\n"
for fold in train test; do
    echo "$0: extracting $fold embeddings.."
    command="echo \${${fold}_set}"
    data_fold=$(eval $command)

    local/calculate_for_each_subsegment.sh --cmd "$train_cmd" --nj $nj \
        --apply-cmn true ${nnet_dir} ${data_fold} ${xvec_fold}/${fold}

    merge_dir=${out_fold}/${fold}
    mkdir -p $merge_dir
    python3 local/merge_arks.py ${data_fold} ${xvec_fold}/${fold} ${merge_dir}
    echo 50 > ${merge_dir}/ivector_period
done

echo -e "\n$0: successfully extracted xvector data.."
exit 0
