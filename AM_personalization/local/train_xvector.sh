#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

. ./path.sh
. ./cmd.sh

# assume that xvector data is located under "xvec_data/data_no_sil_speaker"
data_xvector=xvec_data/data_no_sil_speaker

train_stage=-1
use_gpu=true
remove_egs=false
min_chunk_size=50

nnet_dir=nnet_xvec/${data_xvector##*_}
egs_dir=${nnet_dir}/egs

num_pdfs=$(awk '{print $2}' $data_xvector/utt2spk | sort | uniq -c | wc -l)

echo "$0: getting neural network training egs.."
local/get_xvector_egs.sh --cmd "$train_cmd" \
    --nj 8 \
    --stage 0 \
    --frames-per-iter 5000000 \
    --frames-per-iter-diagnostic 1000000 \
    --min-frames-per-chunk 20 \
    --max-frames-per-chunk 400 \
    --num-repeats 100 \
    --num-diagnostic-archives 1 \
    "$data_xvector" $egs_dir

echo "$0: creating neural net configs using the xconfig parser..";
num_targets=$(wc -w $egs_dir/pdf2num | awk '{print $1}')
feat_dim=$(cat $egs_dir/info/feat_dim)

# This chunk-size corresponds to the maximum number of frames the
# stats layer is able to pool over.  In this script, it corresponds
# to 100 seconds. If the input recording is greater than 100 seconds,
# we will compute multiple xvectors from the same recording and average
# to produce the final xvector

max_chunk_size=10000

# The smallest number of frames computing an xvector from
# Note that the hard minimum is given by the left and right context of the
# frame-level layers

min_chunk_size=$window
mkdir -p $nnet_dir/configs
cat <<EOF > $nnet_dir/configs/network.xconfig
# please note that it is important to have input layer with the name=input

# The frame-level layers
input dim=${feat_dim} name=input
relu-batchnorm-layer name=tdnn1 input=Append(-2,-1,0,1,2) dim=512
relu-batchnorm-layer name=tdnn2 input=Append(-2,0,2) dim=512
relu-batchnorm-layer name=tdnn3 input=Append(-3,0,3) dim=512
relu-batchnorm-layer name=tdnn4 dim=512
relu-batchnorm-layer name=tdnn5 dim=1500

# The stats pooling layer. Layers after this are segment-level.
# In the config below, first and last argument (0, and ${max_chunk_size})
# means that we pool over an input segment starting at frame 0
# and ending at frame ${max_chunk_size} or earlier. Other arguments (1:1)
# mean that no subsampling is performed
stats-layer name=stats config=mean+stddev(0:1:1:${max_chunk_size})

# This is where we usually extract the embedding (aka xvector) from
relu-batchnorm-layer name=tdnn6 dim=512 input=stats

# This is where another layer the embedding could be extracted
# from, but usually the previous one works better.
relu-batchnorm-layer name=tdnn7 dim=512
output-layer name=output include-log-softmax=true dim=${num_targets}
EOF

steps/nnet3/xconfig_to_configs.py \
    --xconfig-file $nnet_dir/configs/network.xconfig \
    --config-dir $nnet_dir/configs/
cp $nnet_dir/configs/final.config $nnet_dir/nnet.config

# these three files will be used by sid/nnet3/xvector/extract_xvectors.sh
echo "output-node name=output input=tdnn6.affine" > $nnet_dir/extract.config
echo "$max_chunk_size" > $nnet_dir/max_chunk_size
echo "$min_chunk_size" > $nnet_dir/min_chunk_size

dropout_schedule='0,0@0.20,0.1@0.50,0'
srand=123
steps/nnet3/train_raw_dnn.py --stage=$train_stage \
    --cmd="$train_cmd" \
    --trainer.optimization.proportional-shrink 10 \
    --trainer.optimization.momentum=0.5 \
    --trainer.optimization.num-jobs-initial=1 \
    --trainer.optimization.num-jobs-final=2 \
    --trainer.optimization.initial-effective-lrate=0.001 \
    --trainer.optimization.final-effective-lrate=0.0001 \
    --trainer.optimization.minibatch-size=64 \
    --trainer.srand=$srand \
    --trainer.max-param-change=2 \
    --trainer.num-epochs=1 \
    --trainer.dropout-schedule="$dropout_schedule" \
    --trainer.shuffle-buffer-size=1000 \
    --egs.frames-per-eg=1 \
    --egs.dir="$egs_dir" \
    --cleanup.remove-egs $remove_egs \
    --cleanup.preserve-model-interval=10 \
    --use-gpu=true \
    --dir=$nnet_dir || exit 1

echo "$0: training of xvectors is finished!"
exit 0
