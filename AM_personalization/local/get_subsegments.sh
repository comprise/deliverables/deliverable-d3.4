#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

data=$1
window=50

per=$(echo $window / 100 | bc -l | awk '{printf "%0.2f", $0}')

echo "$0: preparing sub-segmentation for ${data}.."

rm -f ${data}/subsegments
utt2dur=${data}/utt2dur
while IFS= read -r line; do
    fname=`echo ${line} | cut -f1 -d " "`
    dur=`echo ${line} | cut -f2 -d " "`

    ind=1
    for end in $(seq $per $per $dur); do
        oname=${fname}_${ind}
        max_dur=$(seq $window $window $dur | tail -1)
        echo "$oname $fname 0.0 $max_dur"
        ind=$((ind+1))
    done

    oname=${fname}_${ind}
    end=`printf "%0.2f" ${dur}`
    echo "$oname $fname 0.0 $end"
done < "$utt2dur" >> ${data}/subsegments

echo -e "\n$0: segmentation is done"
echo 0
