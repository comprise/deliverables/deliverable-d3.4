#!/bin/bash
#
# Copyright (C) 2021, COMPRISE, http://compriseh2020.eu
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# locate your 'lang' files
lang_model=folder/for/language/modeling

# set 'nj' according to your computational effort
nj=4

# absolute path for traing and test files
trainSplit=folder/for/train
testSplit=folder/for/test

graph_folder=graph

split_stage=true        # mono/tri1/tri2 training spliting
mono_stage=true         # mono training step
tri1_stage=true         # tri1 training step
tri2_stage=true         # tri2 training step
tri3_stage=true         # tri3 on partial trainset
tri3_full_stage=true    # tri3 on full trainset

# Acoustic Model Parameters (tested for 40hrs dataset)
numLeavesTri1=2000
numGaussTri1=10000
numLeavesTri2=2500
numGaussTri2=15000
numLeavesTri3=2500
numGaussTri3=15000
numLeavesTri3Full=3500
numGaussTri3Full=25000

# Number Of Utterances For Different AM
monoSize=5000           # shortest
tri1Size=5000           # random
tri2Size=10000          # random

. ./path.sh          # set paths to binaries
. ./cmd.sh           # for the execution queue

set -e

. utils/parse_options.sh

if $split_stage; then
    echo -e "==============================================================="
    echo "         Create 'Mono/Tri1/Tri2' Splits for Train"
    echo -e "===============================================================\n"

    # make subset with shortest $monoSize utterances for mono models (use 5k)
    utils/subset_data_dir.sh --shortest data/split/$trainSplit $monoSize data/split/$trainSplit"_s"$monoSize || exit 1

    # make subset with random $tri1Size utterances for tri1 models (use 5k)
    utils/subset_data_dir.sh data/split/$trainSplit $tri1Size data/split/$trainSplit"_r"$tri1Size || exit 1

    # make subset with random $tri2Size utterances for tri2 models (use 10k)
    utils/subset_data_dir.sh data/split/$trainSplit $tri2Size data/split/$trainSplit"_r"$tri2Size || exit 1
fi
echo -e "\n--> [END] of Creating 'Mono/Tri1/Tri2' Splits for Train\n"

if $mono_stage; then
    echo -e "==============================================================="
    echo "              MonoPhone Training & Decoding"
    echo -e "===============================================================\n"

    steps/train_mono.sh --nj "$nj" --cmd "$train_cmd" data/split/$trainSplit"_s"$monoSize data/lang exp/mono

    utils/mkgraph.sh $lang_model exp/mono exp/mono/graph
fi
echo -e "\n--> [END] of MonoPhone Training & Decoding\n"

if $tri1_stage; then
    echo -e "==============================================================="
    echo "       tri1 : Deltas + Delta-Deltas Training & Decoding"
    echo -e "===============================================================\n"

    # Align Train_r5k Data With Mono Models
    steps/align_si.sh --boost-silence 1.25 --nj "$nj" --cmd "$train_cmd" data/split/$trainSplit"_r"$tri1Size data/lang exp/mono exp/mono_ali

    # Using Monophone Model, Train 'tri1'
    steps/train_deltas.sh --cmd "$train_cmd" $numLeavesTri1 $numGaussTri1 data/split/$trainSplit"_r"$tri1Size data/lang exp/mono_ali exp/tri1

    utils/mkgraph.sh $lang_model exp/tri1 exp/tri1/graph
fi
echo -e "\n--> [END] of Training 'tri1'\n"

if $tri2_stage; then
    echo -e "==============================================================="
    echo "          tri2 : LDA + MLLT Training & Decoding"
    echo -e "===============================================================\n"

    # Align Train_r10k Data With 'tri1' Models
    steps/align_si.sh --nj "$nj" --cmd "$train_cmd" data/split/$trainSplit"_r"$tri2Size data/lang exp/tri1 exp/tri1_ali

    # Using 'tri1' Model, Train 'tri2'
    steps/train_lda_mllt.sh --cmd "$train_cmd" --splice-opts "--left-context=3 --right-context=3" $numLeavesTri2 $numGaussTri2 data/split/$trainSplit"_r"$tri2Size data/lang exp/tri1_ali exp/tri2

    utils/mkgraph.sh $lang_model exp/tri2 exp/tri2/graph
fi
echo -e "\n--> [END] of Training 'tri2'\n"

if $tri3_stage; then
    echo -e "==============================================================="
    echo "        tri3 : LDA + MLLT + SAT Training & Decoding"
    echo -e "===============================================================\n"

    # Align Train_r10k Data With 'tri2' Models
    steps/align_si.sh --nj "$nj" --cmd "$train_cmd" --use-graphs true data/split/$trainSplit"_r"$tri2Size data/lang exp/tri2 exp/tri2_ali

    # Using 'tri2' Model, Train 'tri3'
    steps/train_sat.sh --cmd "$train_cmd" $numLeavesTri3 $numGaussTri3 data/split/$trainSplit"_r"$tri2Size data/lang exp/tri2_ali exp/tri3

    utils/mkgraph.sh $lang_model exp/tri3 exp/tri3/graph
fi
echo -e "\n--> [END] of Training 'tri3'\n"

if $tri3_full_stage; then
    echo -e "==============================================================="
    echo "      tri3_full: LDA + MLLT + SAT Training & Decoding"
    echo -e "===============================================================\n"

    # Align Full Train Data With 'tri3' Models
    steps/align_si.sh --nj "$nj" --cmd "$train_cmd" --use-graphs true data/split/$trainSplit data/lang exp/tri3 exp/tri3_ali

    # Using 'tri3' Model, Train 'tri3_full'
    steps/train_sat.sh --cmd "$train_cmd" $numLeavesTri3Full $numGaussTri3Full data/split/$trainSplit data/lang exp/tri3_ali exp/tri3_full

    utils/mkgraph.sh $lang_model exp/tri3_full exp/tri3_full/${graph_folder}

    steps/decode_fmllr.sh --nj "$nj" --cmd "$decode_cmd" \
        exp/tri3_full/${graph_folder} data/split/$testSplit \
        exp/tri3_full/decode_${graph_folder}
fi

echo -e "\n--> [SUCCESSFUL] 'train_tri3.sh'"
exit 0
